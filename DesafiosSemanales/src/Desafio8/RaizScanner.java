package Desafio8;

import java.util.Scanner;

public class RaizScanner {

    public static void main(String[] args){

        Scanner sc= new Scanner(System.in);
        System.out.println("Introduzca un número para obtener su raiz cuadrada:");
        int numero = sc.nextInt();
        double raiz = Math.sqrt(numero);
        System.out.println("La raiz de "+ numero+" es = "+raiz);
        sc.close();
    }
}
