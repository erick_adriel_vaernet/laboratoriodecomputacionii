package Desafio11;

import java.util.Random;

public class CuentaCorriente {

    private double saldo;
    private String nombreTitular;
    private long numeroCuenta;

    //Constructor de la clase CuentaCorriente
    public CuentaCorriente (String nombreTitular, double saldo){

        this.nombreTitular = nombreTitular;
        this.saldo = saldo;

        Random aleatorio = new Random();
        this.numeroCuenta = Math.abs(aleatorio.nextLong());

    }

    //2do Constructor de la clase CuentaCorriente (Sobrecarga)
    public CuentaCorriente (String nombreTitular){

        this.nombreTitular = nombreTitular;
        this.saldo=0;

        Random aleatorio = new Random();
        this.numeroCuenta = Math.abs(aleatorio.nextLong());

    }


    public double getSaldo (){
        return this.saldo;
    }

    public void ingresarDinero(double dinero){
    	 if(dinero >0) {
             this.saldo += dinero;
             System.out.println("Se ingreso $"+ dinero+" a la cuenta N�"+this.numeroCuenta+" perteneciente a:"+this.nombreTitular); 
         }else {
         	System.out.println("No se puede ingresar un valor negativo");
         }

    }
    public void sacarDinero(double dinero){
    	if(dinero >0) {
            this.saldo -= dinero;
            System.out.println("Se retiro $"+ dinero+" de la cuenta N�"+this.numeroCuenta+" perteneciente a:"+this.nombreTitular);
        }else {
            System.out.println("No se puede ingresar un valor negativo");
        }
    }

    //Este metodo retorna un string con el valor de todos los atributos del objeto
    public String toString() {
        return "\n nombreTitular = " + nombreTitular+
                "\n saldo = " + saldo +
                "\n numeroCuenta = " + numeroCuenta + "\n";
    }

    //Metodo propio de la lase para realizar transferencias de dinero una cuenta a otra.
    public static void transferencia(CuentaCorriente cuenta1, CuentaCorriente cuenta2, double dinero){
        cuenta1.saldo-=dinero;
        cuenta2.saldo+=dinero;
    }

}
