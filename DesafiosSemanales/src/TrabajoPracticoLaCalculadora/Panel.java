package TrabajoPracticoLaCalculadora;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JPanel;

public class Panel extends JPanel{
	
	private static final long serialVersionUID = 1L;
	
	private JButton pantalla =new JButton("0");
    private JPanel panelNumeros;
    private boolean comienzo=true;
    private String operacion;
    private double total;

    
    public Panel() {
    	setLayout(new BorderLayout());
        pantalla.setEnabled(false);
        add(pantalla,BorderLayout.NORTH);
        panelNumeros = new JPanel();
        panelNumeros.setLayout(new GridLayout(4,4));
        limpiarPantalla limpiador = new limpiarPantalla();
        anadirNumeros escuchador = new anadirNumeros();
        Operadores operador = new Operadores();

        
        //Primer fila
        crearBotones("7", escuchador);
        crearBotones("8", escuchador);
        crearBotones("9", escuchador);
        crearBotones("x", operador);
        
        //Segunda fila
        crearBotones("4", escuchador);
        crearBotones("5", escuchador);
        crearBotones("6", escuchador);
        crearBotones("-", operador);
        
        //Tercer fila
        crearBotones("1",escuchador);
        crearBotones("2",escuchador);
        crearBotones("3",escuchador);
        crearBotones("+", operador);

        //Cuarta fila
        crearBotones("/", operador);
        crearBotones("0",escuchador);
        crearBotones("AC", limpiador);
        crearBotones("=", operador);

        add(panelNumeros);
        operacion="=";

    }
    
    
    private class anadirNumeros implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            String numero = e.getActionCommand();
            if(comienzo){
                pantalla.setText("");
                comienzo=false;

            }
            pantalla.setText(pantalla.getText()+numero);
        }
    }

    private class Operadores implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            String usuario = e.getActionCommand();
            calcular(Double.parseDouble(pantalla.getText()));
            operacion=usuario;
            comienzo=true;

        }

        public void calcular(Double numero){
            try{
                if (operacion.equals("+")){
                    total+=numero;
                } else if (operacion.equals("-")){
                    total-=numero;
                } else if (operacion.equals("x")){
                    total*=numero;
                } else if (operacion.equals("/")){
                    total/=numero;
                } else if (operacion.equals("=")){
                    total=numero;
                } else if (operacion.equals("AC")){
                    total=0;
                } 
                pantalla.setText(""+total);
            } catch (ArithmeticException e){
                System.out.println("No se puede dividir por 0");
            }

        }
    }
    private class limpiarPantalla implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            pantalla.setText("0");
        }
    }

    private void crearBotones(String nombre, ActionListener escuchador){
        JButton btn = new JButton(nombre);
        btn.addActionListener(escuchador);
        panelNumeros.add(btn);

    }



	
}
