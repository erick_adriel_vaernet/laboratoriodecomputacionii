package Desafio9;

import java.util.Scanner;

public class AlturaIdeal {

    public static void main(String[] args){
        Scanner sc= new Scanner(System.in);

        System.out.println("**Este programa calcula su altura ideal**");

        System.out.println("Indique su género (M = masculino / F = femenino):");
        char genero = sc.next().charAt(0);

        //En caso de introducir un caracter diferente a 'M', 'm', 'F', 'f' se vuelve a pedir el genero
        while (genero!='M' & genero!='m' & genero!='F' & genero!='f') {
            System.out.println("Se introdujo un carácter no valido, indique su género  con la letra 'M' o 'm' para masculino y  'F' o 'f' para femenino:");
            genero = sc.next().charAt(0);
        }


        System.out.println("Indique su altura en cm:");
        float altura = sc.nextFloat();

        //En caso de una altura igual o menor a 110 en el caso del hombre ,o altura menor o igual a 120 en el caso de la mujer, se vuelve a pedir la altura
        //Esto se realiza para no obtener 0 o valores negativos como resultado en pesoIdeal
        while ((genero=='M' && altura<=110 || genero=='m' && altura<=110) || (genero=='F' && altura<=120 || genero=='f' && altura<=120))
        {
            System.out.println("Se introdujo un valor erroneo como altura en cm, intente usando una altura mayor a 110 en el caso del hombre y mayor a 120 en el caso de la mujer :");
            altura = sc.nextFloat();
        }

        float pesoIdeal=altura;

        if (genero=='M'||genero=='m'){
            pesoIdeal -= 110;
        }
        else if(genero=='F'||genero=='f') {
            pesoIdeal -=  120;
        }

        System.out.println("Su peso ideal es "+ pesoIdeal +"Kg");
        
        sc.close();

    }
}
