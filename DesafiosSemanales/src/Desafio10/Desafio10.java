package Desafio10;

import java.util.Scanner;

//Este es un programa en el cual se genera un número aleatorio entre 1 y 100, el usuario debera adivianar cual es el número con un  maximo de 10 intentos
public class Desafio10 {

    public static void main(String[] args){

        Scanner sc= new Scanner(System.in);

        int chosenNum=-1;
        int intentos=0;

        //Se genera el numero aleatorio con funcion Math.random() que nos genera un número entre 0.0 y 1.0;
        // Luego se multiplica ese número por 100 para generar un número aleatorio entre 1 y 100; y por último se pasa este a tipo de dato entero (int)
        int randomNum= (int) (Math.random()*100);

        //Se genera un bucle en el cual el usuario debe adivinar el numero random para salir del bucle
        while (chosenNum!=randomNum){

            //Se va incrementando los intentos cada vez que se introduce un número por consola;
            intentos++;
            System.out.println("Intente adivinar el número entre 1 y 100 (intento "+intentos+"): ");
            chosenNum=sc.nextInt();

            //Se emite un mensaje en el caso de que el número elegido sea mayor o menor que el random
            if(chosenNum<randomNum) System.out.println("El número random es mayor que el número eligido");
            else if (chosenNum>randomNum) System.out.println("El número random es menor que el número eligido");
        }

        //En caso deque el usuario adivine se emite un mensaje anunciando que gano y el número de intentos que le llevo;
        System.out.println("¡Adivinaste!: El número en cuestion era "+chosenNum+" lo resolviste en "+intentos+" intentos");
        sc.close();
    }

}
