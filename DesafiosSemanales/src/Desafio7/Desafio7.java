package Desafio7;

public class Desafio7 {
    public static void main(String[] args){

        System.out.println("Funciones trigonometricas:");
        System.out.println("- El seno de 0.7 es "+Math.sin(0.7));
        System.out.println("- El coseno de 1.6 es "+Math.cos(1.6));
        System.out.println("- La tangente  de 50 es "+Math.tan(50));
        System.out.println("- El arcotangente de 29 es "+Math.atan(29));
        System.out.println("- Usando la función Math.atan2() con los valores 29 y 30 se obtiene "+Math.atan2(29,30));

        System.out.println("-------------------------------------------------------------------------------------");

        System.out.println("Funciones exponencial y su inversa:");
        System.out.println("- 4 al cuadrado es  "+Math.exp(4));
        System.out.println("- El logaritmo de 11 es "+Math.log(11));

        System.out.println("-------------------------------------------------------------------------------------");

        System.out.println( "Las dos constantes PI y e:");
        System.out.println("- Pi vale "+ Math.PI);
        System.out.println("- E vale "+ Math.E);
    }
}
