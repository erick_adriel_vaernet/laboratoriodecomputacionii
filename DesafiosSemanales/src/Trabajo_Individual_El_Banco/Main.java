package Trabajo_Individual_El_Banco;

import java.util.Scanner;
import java.util.Set;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashSet;

public class Main {

	public static void main(String[] args) {

		Boolean continuar= true;
		Scanner sc= new Scanner(System.in);
		Set<CuentaCorriente> listaCuentasCorrientes= new HashSet<>();
		
		System.out.println("**Bienvenido al Banco UTN-FRRE**");
		
		while(continuar==true) {
			System.out.println("\n--------------------------------------------------------------");
			System.out.println("*Menu Principal: Elija entre las opciones disponibles:\n");
			System.out.println("1-Agregar cuentas al Banco");//Crea una cuenta en el programa
			System.out.println("2-Eliminar cuentas del Banco");//elimina una cuenta en el programa
			System.out.println("3-Realizar una transfernecia entre cuentas");//realizar transferencia entre cuentas
			System.out.println("4-Listar cuentas del Banco cargadas en el programa (Temporales)"); //Lista las cuentas en el programa (se encuentran en ram)
			System.out.println("5-Almacenar en el archivo las cuentas cargadas Banco (Permanentes)"); //Almacena las cuentas cargadas del programa en un archivo
			System.out.println("6-Traer cuentas almacenadas en el archivo y listarlas (Permanentes)");// Lista las cuentas cargadas en el archivo
			System.out.println("7-Salir");
			System.out.println("--------------------------------------------------------------\n");
			
			// Se usa manejo de excepciones en caso de que el usuario cometa algun error, a su vez se rodea este con do-while, de esta manera si el usuario se equivoca se vuelve a pedir la respuesta  
			Boolean error1=false;
			int resp=-1;

			do {
				try {
					
					System.out.println("Opcion:");
					resp = sc.nextInt();
					
					
				}catch(Exception e) {
					System.out.println("***Error: Solo se admite un numero como respuesta (reintente ingresando un numero entre 1 y 6 segun las opciones)");
					error1=true;
					e.printStackTrace();
				}				
			}while (error1==true); 
			
			
			Boolean seguir=true;
			
			switch(resp){
				case 1:
					System.out.println("\nSelecciono agregar cuentas al banco");
					String nombre;
					double saldo;
					int nCuenta;
					do {
						try {
							System.out.println("Ingrese el nombre del titular de la cuenta:");						
							nombre = sc.next();
							
							System.out.println("Ingrese el Saldo inicial de la cuenta:");						
							saldo = sc.nextDouble();
							
							System.out.println("Ingrese el numero de cuenta:");
							nCuenta = sc.nextInt();
							
							CuentaCorriente nuevaCuenta= new CuentaCorriente(nombre, saldo, nCuenta);
							listaCuentasCorrientes.add(nuevaCuenta);
							
						}catch(Exception e) {
							System.out.println("***Error en el ingreso de los Valores: Reingrese los valores adecuadamente");
							seguir=false;
							e.printStackTrace();
						}
						
						try {
							System.out.println("Desea agregar otra cuenta?(si/no):");
							if(sc.next().toLowerCase().equals("si")) seguir=true;
							else seguir=false;
							
								
						}catch(Exception e) {
							System.out.println("Ingreso un valor erroneo, el programa por defecto entiende que no quiere seguir agregando cuentas");
							seguir=false;
							e.printStackTrace();
						}
						
					}while (seguir); 
					
					break;
				case 2:
					int elimCuenta;
					Boolean check;
					
					do {
						try {
							System.out.println("\nSelecciono eliminar una cuenta, a continuacion se lisitan las cuentas cargadas en el programa actualmente:");
							
							for(CuentaCorriente cuenta:listaCuentasCorrientes) {
								System.out.println("-"+cuenta.toString());
							}
							
							System.out.println("Ingrese el numero de cuenta de la cuenta que quiere eliminar:");
							elimCuenta=sc.nextInt();
							
							check=false;
							for(CuentaCorriente cuenta:listaCuentasCorrientes) {
								if(cuenta.getNumeroCuenta()==elimCuenta) {
									System.out.println("Se elimino La cuenta:\n"+cuenta.toString());
									listaCuentasCorrientes.remove(cuenta);
									check=true;
								}					
							}
							
							if(!check) {
								System.out.println("No se encontro la cuenta que desea eliminar");
								
							}
							
						}catch(Exception e) {
							System.out.println("***Error en el ingreso de los Valores: Reingrese los valores adecuadamente");
							seguir=false;
							e.printStackTrace();
							
						}
						
						try {
							System.out.println("�Desea eliminar  otra cuenta?(si/no):");
							if(sc.next().toLowerCase().equals("si")) seguir=true;
							else seguir=false;
								
						}catch(Exception e) {
							System.out.println("Ingreso un valor erroneo, el programa por defecto entiende que no quiere seguir agregando cuentas");
							seguir=false;
							e.printStackTrace();
						}
						
					}while(seguir);
					break;
				case 3:
					int nCuenta1;
					int nCuenta2;
					double dineroT;
					Boolean check1;
					Boolean check2;
					CuentaCorriente cuenta1= null;
					CuentaCorriente cuenta2 = null;
					
					do {
						try {
							System.out.println("\nSelecciono Transferencia entre cuentas, a continuacion se lisitan las cuentas cargadas en el programa actualmente:");
														
							for(CuentaCorriente cuenta:listaCuentasCorrientes) {
								System.out.println("- "+cuenta.toString());
							}
							
							System.out.println("Ingrese el numero de cuenta de origen para la transferencia:");
							nCuenta1=sc.nextInt();

							System.out.println("Ingrese el numero de cuenta de destino para la transferencia:");
							nCuenta2=sc.nextInt();

							System.out.println("Ingrese monto a transferir $:");
							dineroT=sc.nextDouble();
							
							check1=false;
							check2 =false;
							
							for(CuentaCorriente cuenta:listaCuentasCorrientes) {
								if(cuenta.getNumeroCuenta()==nCuenta1) {
									cuenta1= cuenta;
									check1=true;
								}
								
								if(cuenta.getNumeroCuenta()==nCuenta2) {
									cuenta2= cuenta;
									check2=true;
								}
							}
							
							if(check1 && check2) {
								CuentaCorriente.transferencia(cuenta1, cuenta2, dineroT);
							}
							
						}catch(Exception e) {
							System.out.println("***Error en el ingreso de los Valores: Reingrese los valores adecuadamente");
							seguir=false;
							e.printStackTrace();
						}
						
						try {
							System.out.println("�Desea realizar otra transferencia?(si/no):");
							if(sc.next().toLowerCase().equals("si")) seguir=true;
							else seguir=false;
								
						}catch(Exception e) {
							System.out.println("Ingreso un valor erroneo, el programa por defecto entiende que no quiere seguir realizando transferencias");
							seguir=false;
						}
						
					}while(seguir);
				
					break;
				case 4:
					System.out.println("Cuentas cargadas en el programa actualmente:");
					for(CuentaCorriente cuenta:listaCuentasCorrientes) {
						System.out.println("- "+cuenta.toString());
					}
					System.out.println("\n*Recuerde que estas cuentas se encuentran en el programa. "
							+ "Para almacenarlas permanentemente en un archivo seleccione la opcion 5 del menu principal");
					System.out.println("\n-Presione una tecla cualquiera para volver al menu principal:");
					sc.next();
					
					break;
				case 5:
					System.out.println("\nSelecciono almacenar las cuentas del banco");
				
					try {
						
					ObjectOutputStream fSalida= new ObjectOutputStream(new FileOutputStream("src"+File.separator+"Trabajo_Individual_El_Banco"+File.separator+"CuentasCorrientes.dat"));
					fSalida.writeObject(listaCuentasCorrientes);
					fSalida.close();
					System.out.println("*Archivo Guardado Exitosamente*");
		
					} catch (FileNotFoundException e) {
						e.printStackTrace();
						
					} catch (IOException e) {
						e.printStackTrace();
					}
					
					
					break;
				case 6:
					System.out.println("\nSelecciono listar las cuentas del banco almacenadas:");
				
				ObjectInputStream fEntrada;
				try {
					fEntrada = new ObjectInputStream(new FileInputStream("src"+File.separator+"Trabajo_Individual_El_Banco"+File.separator+"CuentasCorrientes.dat"));
					
					@SuppressWarnings("unchecked")
					Set <CuentaCorriente> cuentasArchivadas= (Set<CuentaCorriente>) fEntrada.readObject();
					for(CuentaCorriente cuenta:cuentasArchivadas) {
						System.out.println("- "+cuenta.toString());
					}
					fEntrada.close();
					
				} catch (Exception e) {
					e.printStackTrace();
				}
								
					break;
				case 7:
					System.out.println("*Se procedera con la salida del programa...");
					continuar=false;
					break;
				case -1:System.out.println("*:Error en el Programa*");//nunca deberia ejecutarse ya que tenemos el try-catch, pero por las dudas no esta de mas preveer cualquier inconveniente
					break;
				default: 
					System.out.println("*Elegiste un numero que no esta en el rango de las opciones(1-6), ingrese un valor valido*");
					break;
			}
			
	
		//while fin
		}
		
	sc.close();
	//main fin	
	}

}
