package Trabajo_Individual_El_Banco;


import java.io.Serializable;
import java.util.Random;

public class CuentaCorriente implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private double saldo;
    private String nombreTitular;
    private long numeroCuenta;

    //Constructor de la clase CuentaCorriente
    public CuentaCorriente (String nombreTitular, double saldo, int numeroCuenta){

        this.nombreTitular = nombreTitular;
        this.saldo = saldo;

        this.numeroCuenta =numeroCuenta;

    }
    
    //2do Constructor de la clase CuentaCorriente, en caso de que no se pase el numero de cuenta, se genera uno aleatoriamente (sobrecarga del constructor)
    public CuentaCorriente (String nombreTitular, double saldo){

        this.nombreTitular = nombreTitular;
        this.saldo = saldo;

        Random aleatorio = new Random();
        this.numeroCuenta = Math.abs(aleatorio.nextLong());

    }

    //---------------------------------------------Getters and Setters------------------------------------------------ 
    
    public String getNombreTitular() {
		return nombreTitular;
	}

	public void setNombreTitular(String nombreTitular) {
		this.nombreTitular = nombreTitular;
	}
	
	public long getNumeroCuenta() {
		return numeroCuenta;
	}

	public void setNumeroCuenta(long numeroCuenta) {
		this.numeroCuenta = numeroCuenta;
	}

	
	public double verSaldo (){
        return this.saldo;
    }

    public void ingresarDinero(double dinero){
        if(dinero >0) {
            this.saldo += dinero;
            System.out.println("Se ingreso $"+ dinero+" a la cuenta N�"+this.numeroCuenta+" perteneciente a:"+this.nombreTitular); 
        }else {
        	System.out.println("No se puede ingresar un valor negativo");
        }
    }
    
    public void sacarDinero(double dinero){
    	if(dinero >0) {
            this.saldo -= dinero;
            System.out.println("Se retiro $"+ dinero+" de la cuenta N�"+this.numeroCuenta+" perteneciente a:"+this.nombreTitular);
        }else {
            System.out.println("No se puede ingresar un valor negativo");
        }
    }
    
   
    //-----------------------------------------Metodo est�tico para las transferencias------------------------------------
    
    //Metodo propio de la clase para realizar transferencias de dinero una cuenta a otra.
    public static void transferencia(CuentaCorriente cuenta1, CuentaCorriente cuenta2, double dinero){
    	if(dinero >0) {
    		
    		cuenta1.saldo-=dinero;
    		cuenta2.saldo+=dinero;
    		
            System.out.println("Se transfirio $"+ dinero+" de la cuenta:\n "+
    		cuenta1.toString()+
    		"\n a la cuenta:"+
    		cuenta2.toString());
            
        }else {
            System.out.println("No se puede ingresar un valor negativo");
        }    	    	
    }

    //--------------------------------------------------------------toString--------------------------------------------------
    @Override
    //Este metodo retorna un string con el valor de todos los atributos del objeto
    
    public String toString() {
        return "CuentaCorriente:{"+
        		" nombreTitular: " + nombreTitular+
                ", saldo: " + saldo +
                ", numeroCuenta: " + numeroCuenta +
                " }";
    }

    
}

