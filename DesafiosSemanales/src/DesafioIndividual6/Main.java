package DesafioIndividual6;

import javax.swing.JFrame;

public class Main {

	public static void main(String[] args) {

		/*
		Crear un programa con interface gr�fica que contenga un marco con un panel en su interior.
	    Al hacer clic con el rat�n en el panel, debe salir un mensaje de texto en consola que diga que
	    se ha producido alg�n evento.El programa debe contener tambi�n por lo menos 2 elementos gr�ficos
	    que tengan la cualidad de escribir en la consola cuando alguno ha ganado el foco y se manifieste
	    con un mensaje como "el elemento ha ganado el foco".
		*/
		
		Ventana ventanaPrincipal = new Ventana();
		
		ventanaPrincipal.setVisible(true);
		
		ventanaPrincipal.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		
	}
	
	
}
