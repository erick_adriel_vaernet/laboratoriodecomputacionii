package DesafioIndividual6;

import java.awt.Graphics;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class Panel extends JPanel{
	
	private static final long serialVersionUID = 1L;

	JTextField campo1;
	JTextField campo2;
	
	
	public Panel() {
		super();
		EventoMouse eventoRaton = new EventoMouse();
		addMouseListener(eventoRaton);		
		
	}
	
	public void paintComponent(Graphics g) {
		
		super.paintComponent(g);
		setLayout(null);
		
		g.drawString("Campo 1", 100, 10);
		campo1= new JTextField();
		campo1.setBounds(100, 20, 150, 20);
		

		g.drawString("Campo 2", 100, 55);
		campo2= new JTextField();
		campo2.setBounds(100, 60, 150, 20);
		
		add(campo1);
		add(campo2);
		
		campo1.addFocusListener(new Foco("Campo 1"));
		campo2.addFocusListener(new Foco("Campo 2"));
		
		
		
	}
	
	private class Foco implements FocusListener {
		String nombreCampo;
		

		public Foco (String nombreCampo) {
			this.nombreCampo = nombreCampo;
		}

		@Override
		public void focusGained(FocusEvent e) {
			System.out.println("El elemento "+nombreCampo +" de texto, ah ganado el Foco");
			
		}

		@Override
		public void focusLost(FocusEvent e) {
			// TODO Auto-generated method stub
			
		}
		
	}
	
		
	private class EventoMouse implements MouseListener{

		@Override
		public void mouseClicked(MouseEvent e) {
			System.out.println("Esta haciendo click en el panel");
			
		}

		@Override
		public void mousePressed(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void mouseReleased(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void mouseEntered(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void mouseExited(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}
		
	}
	
	
}
